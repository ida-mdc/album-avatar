package mdc.ida.hips;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.robot.Robot;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HIPSGhost extends Application implements HIPSAvatarServer.HIPSEventListener {

    private final int sideMove = 100;
    private int sideMoveCurrent = 50;
    private boolean moveLeft = true;
    private double posX, posY;
    private Robot robot;
    private Text chatMessage;
    private ImageView ghostImage;
    private VBox textBox;
    private VBox ghostBox;
    private VBox mainBox;
    private Stage stage;
    private Stage primaryStage;
    private Scene scene;
    private ObservableList<String> citations = FXCollections.observableArrayList();
    private Text citationText;

    @Override
    public void start(Stage primaryStage) {

        HIPSAvatarOptions.Values options = parseOptions(getParameters().getRaw());

        startAndCatchSocketServer(options.port());


        robot = new Robot();

        this.primaryStage = primaryStage;
        stage = createStage(primaryStage);
        mainBox = new VBox();
        ghostBox = new VBox();
        textBox = new VBox();
        chatMessage = new Text("BOOH!");
        attachChatBubble(textBox, chatMessage);
        ghostImage = new ImageView(createGhost());
        ghostBox.getChildren().add(ghostImage);
        mainBox.getChildren().add(textBox);
        mainBox.getChildren().add(ghostBox);
        mainBox.setAlignment(Pos.BOTTOM_CENTER);
        ScrollPane sp = new ScrollPane(mainBox);
        mainBox.setStyle("-fx-background: transparent; -fx-background-color: transparent; ");
        sp.setStyle("-fx-background: transparent; -fx-background-color: transparent; ");
        scene = createScene(mainBox);
        stage.setScene(scene);
        primaryStage.show();
        stage.show();
        center(stage);
        whiggle(stage);
        avoidMouse(stage);
        dummyText();
    }

    private void displayCitations() {
//        ListView<String> list = new ListView<>();
//        list.setItems(citations);
        citationText = new Text();
        citationText.setWrappingWidth(370);
          ScrollPane sp = new ScrollPane();
//        sp.setContent(list);
        sp.setContent(citationText);
        Stage stage = new Stage();
        stage.initOwner(primaryStage);
        stage.setTitle("Citations");
        VBox box = new VBox();
        box.getChildren().add(sp);
//        box.getChildren().add(citationText);
        stage.setScene(new Scene(box, 400, 450));
        Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
        double x = bounds.getMinX() + (bounds.getWidth() - scene.getWidth()) * 0.9;
        double y = bounds.getMinY() + (bounds.getHeight() - scene.getHeight()) * 0.7;
        stage.setX(x);
        stage.setY(y);
        stage.show();
    }

    private Stage createStage(Stage primaryStage) {
        primaryStage.initStyle(StageStyle.UTILITY);
        primaryStage.setOpacity(0.);
        Stage stage = new Stage();
        stage.initOwner(primaryStage);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setAlwaysOnTop(true);
        primaryStage.sizeToScene();
        primaryStage.setResizable(false);
        return stage;
    }

    private HIPSAvatarOptions.Values parseOptions(List<String> list) {
        HIPSAvatarOptions options = HIPSAvatarOptions.options();
        for (int i = 0; i < list.size(); i++) {
            String each = list.get(i);
            if (each.equals("--port") && list.size() > i+1) {
                options.port(Integer.parseInt(list.get(i+1)));
            }
        }
        return options.values;
    }

    private void startAndCatchSocketServer(int port) {
        new Thread(() -> {
            try {
                HIPSAvatarServer server=new HIPSAvatarServer();
                server.listeners().add(this);
                server.start(port);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void attachChatBubble(VBox box, Text chatMessage) {
        VBox textBox = new VBox();
        VBox styleBox = new VBox();
        chatMessage.setWrappingWidth(170);
        Font font = Font.font("Ubuntu", FontWeight.BOLD, FontPosture.REGULAR, 15);
        chatMessage.setFont(font);
        chatMessage.setLineSpacing(1.25);
        textBox.setBackground(new Background(new BackgroundFill(new Color(1, 1, 1, 1), null, null)));
        textBox.getChildren().add(chatMessage);
        textBox.getStyleClass().add("chat-top");
        styleBox.getStyleClass().add("chat-bottom");
        box.getChildren().add(textBox);
        box.getChildren().add(styleBox);
    }

    private Scene createScene(VBox box) {
        final Scene scene = new Scene(box);
        scene.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());
        scene.setFill(null);
        return scene;
    }

    private void center(Stage stage) {
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        double posX = (primScreenBounds.getWidth() - stage.getWidth()) / 2;
        double posY = (primScreenBounds.getHeight() - stage.getHeight()) / 2;
        updateTarget(posX, posY);
        stage.setX(this.posX);
        stage.setY(this.posY);
    }

    private void updateTarget(double posX, double posY) {
        this.posX = posX;
        this.posY = posY;
    }

    private Image createGhost() {
        return new Image(getClass().getResource("/ghost.gif").toString());
    }

    private void whiggle(Stage stage) {
        new Timer("move").scheduleAtFixedRate(new TimerTask() {
            public void run() {
                Platform.runLater(() -> {
                    if(sideMoveCurrent < 0) moveLeft = false;
                    if(sideMoveCurrent > sideMove) moveLeft = true;
                    sideMoveCurrent += moveLeft? -1 : 1;
                    stage.setX(stage.getX() + (moveLeft? -1 : 1));
                    double val = (System.nanoTime() % 200) / 200. * 2*Math.PI;
                    stage.setY(stage.getY() + (int)(Math.sin(val) * 2));
                });
            }
        }, 0, 200);
    }

    private void dummyText() {
        new Timer("move").scheduleAtFixedRate(new TimerTask() {
            public void run() {
                Platform.runLater(() -> {
                    String randomText = "adslfk aliu hsdoig hgosidhg dog  adf luaerliha poiewa ,kefh ,ewiurgewu ewew yirg krewauy aku grliwaurgoqiwue 32o ksjf nkeu ri3 rekufz jsurei weorig erogi reogishg reogi eraaore i";
                    String msg = randomText.substring((int) (Math.random() * (randomText.length() - 1)));
                    updateChatMessage(msg);
                    addCitation(msg);
                });
            }
        }, 0, 1000);
    }

    private void updateChatMessage(String substring) {
        chatMessage.setText(substring);
        double mainHeightPrev = mainBox.getHeight();
        mainBox.autosize();
        double mainHeightAfter = mainBox.getHeight();
//        primaryStage.sizeToScene();
        stage.sizeToScene();
        stage.setY(stage.getY() - (mainHeightAfter - mainHeightPrev));
    }

    private void avoidMouse(Stage stage) {
        new Timer("avoid-mouse").scheduleAtFixedRate(new TimerTask() {
            public void run() {
                Platform.runLater(() -> {
                    int mouseX = (int) robot.getMouseX();
                    int mouseY = (int) robot.getMouseY();
                    int stageX = (int) stage.getX();
                    int stageY = (int) stage.getY();
                    double ghostRadiusWidth = ghostImage.getLayoutBounds().getWidth() / 2;
                    double ghostRadiusHeight = ghostImage.getLayoutBounds().getHeight() / 2;
                    int ghostX = (int) ghostBox.getLayoutX() + (int) ghostRadiusWidth;
                    int ghostY = (int) ghostBox.getLayoutY() + (int) ghostRadiusHeight;
                    int difX = stageX + ghostX - mouseX;
                    int difY = stageY + ghostY - mouseY;
                    double abs = Math.sqrt(difX*difX + difY*difY);
                    float scale = 10.f;
                    if(Math.abs(difX) < ghostRadiusWidth && Math.abs(difY) < ghostRadiusHeight) {
                        stage.setX(stageX + difX* (1./abs)*scale);
                        stage.setY(stageY + difY* (1./abs)*scale);
                    }
                });
            }
        }, 0, 50);
    }

    @Override
    public void hipsEventReceived(String content) {

        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode hipsEvent = mapper.readTree(content);
            String name = hipsEvent.get("name").asText();
            String event = hipsEvent.get("event").asText();
            String description = hipsEvent.get("description").asText();
            JsonNode cite = hipsEvent.get("cite");
            if(cite.isArray()) {
                cite.forEach(c -> addCitation(c.toString()));
            } else {
                addCitation(cite.toString());
            }
            StringBuilder msg = new StringBuilder();
            if(event.equals("started"))
                msg.append("Busy running ").append(name).append("src/main");
            if(event.equals("finished"))
                msg.append("I'm done with ").append(name).append(" :)");
            Platform.runLater(() -> updateChatMessage(msg.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addCitation(String cite) {
        System.out.println(cite);
        if(cite == null || cite.isEmpty()) return;
        if(citations.contains(cite)) return;
        if(citations.size() == 0) {
            Platform.runLater(this::displayCitations);
        }
        citations.add(cite);
        Platform.runLater(() -> {
            citationText.setText(citationText.getText() + cite + "\n-\n");
        });
    }

    public static void main(String... args) {
        launch(args);
    }
}
