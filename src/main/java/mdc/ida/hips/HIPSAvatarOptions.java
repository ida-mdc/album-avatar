package mdc.ida.hips;

import org.scijava.optional.AbstractOptions;

public class HIPSAvatarOptions extends AbstractOptions<HIPSAvatarOptions> {

	public final Values values = new Values();
	private static final String portKey = "port";

	public HIPSAvatarOptions() {
	}

	/**
	 * @return Default {@link HIPSAvatarOptions} instance
	 */
	public static HIPSAvatarOptions options()
	{
		return new HIPSAvatarOptions();
	}

	/**
	 * @param port Which port to use to start a TCP server
	 */
	public HIPSAvatarOptions port(int port) {
		return setValue(portKey, port);
	}

	public class Values extends AbstractValues
	{
		/**
		 * @return Which port to use to start a TCP server
		 */
		public int port() {
			return getValueOrDefault(portKey, 1234);
		}

	}
}
