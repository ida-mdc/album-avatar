package mdc.ida.hips;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class HIPSAvatarServer {

    List<HIPSEventListener> listeners = new ArrayList<>();

    public List<HIPSEventListener> listeners() {
        return listeners;
    }

    public void start(int port) throws IOException {
        ServerSocket serverSocket = null;
        Socket socket = null;

        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();

        }
        while (true) {
            try {
                socket = serverSocket.accept();
            } catch (IOException e) {
                System.out.println("I/O error: " + e);
            }
            // new thread for a client
            new ClientThread(socket).start();
        }


//        serverSocket = new ServerSocket(port);
//        clientSocket = serverSocket.accept();
//        out = new PrintWriter(clientSocket.getOutputStream(), true);
//        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
//        String inputLine;
//        while ((inputLine = in.readLine()) != null) {
////            if (".".equals(inputLine)) {
////                out.println("good bye");
////                break;
////            }
//            System.out.println(inputLine);
//            for (HIPSEventListener listener : listeners) {
//                listener.hipsEventReceived(inputLine);
//            }
//        }
    }

    public class ClientThread extends Thread {
        protected Socket socket;

        public ClientThread(Socket clientSocket) {
            this.socket = clientSocket;
        }

        public void run() {
            InputStream inp;
            BufferedReader brinp;
            try {
                inp = socket.getInputStream();
                brinp = new BufferedReader(new InputStreamReader(inp));
            } catch (IOException e) {
                return;
            }
            String line;
            while (true) {
                try {
                    line = brinp.readLine();
                    if ((line == null) || line.equalsIgnoreCase("QUIT")) {
                        socket.close();
                        return;
                    } else {
                        System.out.println(line);
                        for (HIPSEventListener listener : listeners) {
                            listener.hipsEventReceived(line);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        HIPSAvatarServer server=new HIPSAvatarServer();
        server.start(5000);
    }

    public interface HIPSEventListener {
        void hipsEventReceived(String content);
    }
}
